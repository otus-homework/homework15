﻿using Homework15.Summation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите размер массива: ");
            int arraySize = ArraySizeValidation(Console.ReadLine());

            var array = Generator.GenerateArray(arraySize);

            new CommonSummation(array).Sum();
            new ParallelSummation(array).Sum();
            new PLINQSummation(array).Sum();

            Console.ReadKey();
        }

        private static int ArraySizeValidation(string arraySizeStr)
        {
            int arraySize;
            while (!Int32.TryParse(arraySizeStr, out arraySize) || arraySize == 0)
            {
                Console.WriteLine("Ошибка. Размер массива должен быть больше нуля");
                Console.Write("Введите размер массива: ");
                arraySizeStr = Console.ReadLine();
            }

            return arraySize;
        }
    }
}
