﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework15
{
    class Generator
    {
        public static int[] GenerateArray(int size)
        {
            Console.WriteLine("Генерация массива...");            
            var random = new Random();

            int[] array = new int[size];

            for (int i = 0; i < size; i++)
            {
                array[i] = random.Next();
            }

            Console.WriteLine("Генерация завершена");
            return array;
        }
    }
}
