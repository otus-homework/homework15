﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework15.Summation
{
    class PLINQSummation : ISummation
    {
        private readonly object _locker = new object();
        private readonly int[] _array;
        private long sum = 0;

        public PLINQSummation(int[] array)
        {
            _array = array;
        }

        public void Sum()
        {
            Console.WriteLine("Вычисление суммы с помощью LINQ");

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            _array.AsParallel().ForAll(x => AddSum(x));

            stopWatch.Stop();
            Console.WriteLine($"Сумма элементов: {sum}, время {stopWatch.ElapsedMilliseconds} мс");
        }

        private void AddSum(int item)
        {
            lock (_locker)
            {
                sum += item;
            }
        }
    }
}
