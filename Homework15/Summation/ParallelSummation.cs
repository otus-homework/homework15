﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework15.Summation
{
    class ParallelSummation : ISummation
    {
        private readonly object _locker = new object();
        private readonly int[] _array;
        private long sum = 0;

        public ParallelSummation(int[] array)
        {
            _array = array;
        }
        public void Sum()
        {
            Console.WriteLine("Параллельное вычисление суммы");
            var stopWatch = new Stopwatch();

            stopWatch.Start();
            var result = Parallel.ForEach(_array, AddSum);

            if (result.IsCompleted)
            {
                stopWatch.Stop();
                Console.WriteLine($"Сумма элементов: {sum}, время: {stopWatch.ElapsedMilliseconds} мс");
            }
        }
        private void AddSum(int item)
        {
            lock (_locker)
            {
                sum += item;
            }
        }
    }

}
