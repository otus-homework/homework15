﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework15.Summation
{
    class CommonSummation : ISummation
    {
        private readonly int[] _array;
        private long sum = 0;
        public CommonSummation(int[] array)
        {
            _array = array;
        }
        public void Sum()
        {
            Console.WriteLine("Обычное вычисление суммы");
            var stopWatch = new Stopwatch();

            stopWatch.Start();
            foreach (var item in _array)
            {
                sum += item;
            }
            stopWatch.Stop();

            Console.WriteLine($"Сумма элементов: {sum}, время: {stopWatch.ElapsedMilliseconds} мс");
        }
    }
}
